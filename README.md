# Desafio zup

## Como executar

### Instale o docker
É necessário instalar em sua maquina:
- [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) 
- [Docker-Compose](https://docs.docker.com/compose/install/)

No diretorio do projeto **docker**  execute o comando:
```bash
docker-compose up
```

no arquivo **docker-compose.yaml** estão configurados quatro serviços:
- ws-api-gateway
- ws-client
- mongo
- mongo-express

### Sobre os serviços
#### ws-api-gateway
Serviço feito com Java 12, GraphQL e Spring Webflux
é responsável por direcionar as chamadas para micro serviços especificos de acordo com a requisição recebida através de query resolvers do Graphql, desda forma podendo em uma mesma requisição acessar multiplos serviços da API.
[Repositório do projeto](https://gitlab.com/gustavo-public/zup/ws-api-gateway.git)

``` mermaid
graph TD;
    Resquest-->ApiGateway;
    ApiGateway-->MicroServico1;
    ApiGateway-->MicroServico2;
    ApiGateway-->MicroServicoN;
    MicroServico1-->mongo1;
    MicroServico2-->mongo2;
    MicroServicoN-->mongoN;
``` 

#### ws-client
Micro serviço com Java 12 e Spring webFlux é responsavel por realizar o **CRUD** da entidade Cliente de  forma reativa atendendo as requisições da **api-gateway** ou qualquer outro micro serviço da API.
[Repositório do projeto](https://gitlab.com/gustavo-public/zup/ws-reactive-client.git)

#### mongo
Banco de dados não relacional responsavel por armazenar os dados do micro serviço **ws-client**

#### mongo-express

SGBD Client configurado para acessar o **mongo** permitindo o acompanhamento dos dados inseridos atraves do serviço **ws-client**
 
### Sobre o Desafio
Criar um crud para cliente, explorando todas possibilidades e habilidades possiveis.

### Sobre o Uso
Apos a execução do comando
```bash
docker-compose up
```
será possivel acessar um client do proprio GraphQL no endereço [http://localhost/graphiql](http://localhost/graphiql) onde vc poderá realizar queries (pesquisas) e mutations (criação, edição e remoção)

O mongo express ficará acessível no endereço [http://localhost:8080](http://localhost:8080)

#### Exemplo

Todos estes exemplos podem ser executados através a interface do GraphQL, que possui recurso de *auto-complete*, historico que requisições e documentação sobre as queries e mutations configuradas no schema graqhql.

Porém como solicitado existe um arquivo chamado zup-client.postman_collection.json, que possui algumas colections do postman para serem usadas como teste da api

[Neste link](https://learning.getpostman.com/docs/postman/sending_api_requests/graphql/) você poderá obter todo suport para configurar o 
postman para criar requisições para o GraphQL.

#### Query

- listar todos os clientes

``` bash 
query{
	listClients{
		cpf
		name
		birthDate
		address{
			street
		}
	}
}
``` 

- Buscando por nome (busca do tipo 	like %), retorna lista

``` bash
query{
	  findByName(name: "02"){
				cpf
				name
	  }
}
  ```

 - Busca por CPF

 ``` bash
 query{
 	findByCpf(cpf: "75010690268"){
		cpf
		name
		birthDate
		address{
			state
			street
			town
			number
			neighborhood
			complement
			referencePoint
		}
	}
}
```



##### Mutation
- adicionando um cliente

```bash
mutation{
  addClient(client:{
    	cpf:"98762512013"
    	name: "Client 06"
    	birthDate: "1986-01-02"
    	address: {
        street: "Rua 06"
        state: "AM"
      	number: "6"
        neighborhood: "Bairro 06"
        town: "Cidade 06"
        complement: "apt 06 torre 06"
        referencePoint: "Proximo ao predio 06"
      }
  }){
    name
    cpf
    birthDate
    address{
      state
      street
    }
  }
}

```
 lembrando que em uma requisição **GraphQL** o cliente determina o que deve ser retornado do servidor, logo.
 o corpo da requisição seria
```bash
  addClient(client:{
    	cpf:"98762512013"
    	name: "Client 06"
    	birthDate: "1986-01-02"
    	address: {
        street: "Rua 06"
        state: "AM"
      	number: "6"
        neighborhood: "Bairro 06"
        town: "Cidade 06"
        complement: "apt 06 torre 06"
        referencePoint: "Proximo ao predio 06"
      }
  })
```

e o retorno seria

``` bash
{
    name
    cpf
    birthDate
    address{
      state
      street
    }
  }
```
formando assim o modelo de requisição do **GraphQL**
``` bash
mutation{
	mutationName(params... fieldsOrObjects){
		return fields
	}
}
``` 

- Editando um cliente

```bash
mutation{
  editClient(client:{
    	cpf:"98762512013"
    	name: "Client 06"
    	birthDate: "1986-01-02"
    	address: {
        street: "Rua 06"
        state: "AM"
      	number: "6"
        neighborhood: "Bairro 06"
        town: "Cidade 06"
        complement: "apt 06 torre 06"
        referencePoint: "Proximo ao predio 06"
      }
  }){
    name
    cpf
    birthDate
    address{
      state
      street
    }
  }
}

```
é semelhante ao criar cliente, a unica diferença é o nome da mutation que era **addClient** agora é **editClient**.

- Remover cliente
``` bash
mutation{
   deleteClient(cpf: "75010690268")    
}
``` 
neste caso, remove cliente não possui um corpo como retorno.

### Sobre as tecnologias utilizadas

- spring-boot
- java 12
- junit
- spring webflux
- spring data
- lombok
- graphql
- mongo 
- mongo-express
- maven
- docker
- docker-compose





